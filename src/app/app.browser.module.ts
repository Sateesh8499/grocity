import * as $ from 'jquery';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutes } from '@app/app.routing';
import { AppComponent } from '@app/app.component';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';


import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
// import { MealsComponent } from './meals/meals.component';
// import { SubscribeComponent } from './subscribe/subscribe.component';
// import { CheckoutComponent } from './checkout/checkout.component';
import { FormsModule }   from '@angular/forms';


import { NgxSpinnerModule,NgxSpinnerService  } from "ngx-spinner";
import { ToastrModule } from 'ngx-toastr';
import { AppModule } from './app.module';
 
@NgModule({
  providers:[
    NgxSpinnerService 
  ],
  imports: [
    CommonModule,
    
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    RouterModule.forRoot(AppRoutes),
    CoreModule,
    SharedModule,
    FormsModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    AppModule,
    BrowserTransferStateModule,
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppBrowserModule {}

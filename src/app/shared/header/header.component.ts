import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(public auth: AuthService) {
    if (localStorage.getItem('currentUserProfile')) {
      this.auth.user$ = JSON.parse(localStorage.getItem('currentUserProfile'));
    } else {
      this.auth.user$ = [];
    }
    $(document).ready(function() {
      $(document).click(function(event) {
        var click = $(event.target);
        var _open = $('.navbar-collapse').hasClass('show');
        if (_open === true && !click.hasClass('navbar-toggler')) {
          $('.navbar-toggler').click();
        }
      });
    });
  }
  groceriesStoreName = environment.groceriesStoreId;
  ngOnInit() {}
}

import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import * as $ from 'jquery';
import { WINDOW } from '@ng-toolkit/universal';
declare var jQuery: any;
declare var Swiper: any;
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-cake',
  templateUrl: './cake.component.html',
  styleUrls: ['./cake.component.scss']
})
export class CakeComponent implements OnInit {
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService
  ) {
    this.getStoreDetails();
  }
  storeArr: any = [];
  customerDetailsArr: any = [];

  cakeDetailsArr: any;
  ngOnInit() {
    (function($) {
      'use strict';

      $('.testimonial-slider').slick({
        slidesToShow: 2,
        infinite: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        vertical: true,
        verticalSwiping: true
      });
      $('.testimonial-slider').slick({
        slidesToShow: 2,
        infinite: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        vertical: true,
        verticalSwiping: true
      });

      /* ---------------------------------------------
              owl-carousel
     --------------------------------------------- */

      $('.clients-carousel').owlCarousel({
        loop: false,
        margin: 10,
        autoplay: true,
        nav: false,
        dots: false,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      });

      $('.review').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: false,
        nav: true,
        navText: [
          "<i class='fa fa-chevron-left'></i>",
          "<i class='fa fa-chevron-right'></i>"
        ],
        dots: false,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 2
          }
        }
      });

      /*
      // Counter

      $('.counter').counterUp({
        delay: 10,
        time: 1000
      });




      // Set the date we're counting down to
      var countDownDate = new Date("Dec 25, 2020 24:00:00").getTime();

      // Update the count down every 1 second
      var x = setInterval(function () {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        //document.getElementById("demo").innerHTML = days + "d " + hours + "h "

        $("#demo").html(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");

        // If the count down is finished, write some text
        if (distance < 0) {
          clearInterval(x);
          $("#demo").html("SALE COMPLETED");
        }
      }, 1000);
*/
    })(jQuery);
  }
  getStoreDetails() {
    const data = {
      store_id: environment.cakeStoreId
    };

    this.spinner.show();

    this.store.getStoreDetails(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          if (res.message) {
            this.storeArr = res.message;

            const [key1, value1] = Object.entries(this.storeArr.products)[0];
            this.cakeDetailsArr = this.storeArr.products[key1];

            this.storeArr.bank_options = res.bank_options;
            this.storeArr.delivery_options = res.delivery_options;
            this.storeArr.delivery_days = res.delivery_days;
          } else {
            this.storeArr = [];
          }

          this.spinner.hide();
        } else {
          this.toastr.warning('Something went wrong!');
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}

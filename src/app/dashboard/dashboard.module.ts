import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { DashboardRoutes } from '@app/dashboard/dashboard.routing';
import { HomeComponent } from '@app/dashboard/home/home.component';
import { MealsComponent } from '@app/dashboard/meals/meals.component';
import { SubscribeComponent } from '@app/dashboard/subscribe/subscribe.component';
import { CheckoutComponent } from '@app/dashboard/checkout/checkout.component';
import { SigninComponent } from '@app/dashboard/signin/signin.component';
import { SignupComponent } from '@app/dashboard/signup/signup.component';
import { ForgotPasswordComponent } from '@app/dashboard/forgot-password/forgot-password.component';
import { UserNavigationComponent } from '@app/dashboard/user-navigation/user-navigation.component';
import { ProfileComponent } from '@app/dashboard/profile/profile.component';
import { ChangePasswordComponent } from '@app/dashboard/change-password/change-password.component';
import { OrdersComponent } from '@app/dashboard/orders/orders.component';
import { ManageAddressComponent } from '@app/dashboard/manage-address/manage-address.component';
import { CakeComponent } from '@app/dashboard/cake/cake.component';
import { CakeCheckoutComponent } from '@app/dashboard/cake-checkout/cake-checkout.component';

import { CareersComponent } from '@app/dashboard/careers/careers.component';
import { JobDescriptionComponent } from '@app/dashboard/job-description/job-description.component';

import { MyGroceriesComponent } from '@app/dashboard/my-groceries/my-groceries.component';

import { MyMealsComponent } from '@app/dashboard/my-meals/my-meals.component';

import { OrderReceiptComponent } from '@app/dashboard/order-receipt/order-receipt.component';
import { MealOrderReceiptComponent } from '@app/dashboard/meal-order-receipt/meal-order-receipt.component';

import { VerifyComponent } from '@app/dashboard/verify/verify.component';
import { ResetPasswordComponent } from '@app/dashboard/reset-password/reset-password.component';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CurrencySymbolPipe } from '../currency-symbol.pipe';
import { GroceriesComponent } from './groceries/groceries.component';
import { QuickComponent } from './quick/quick.component';
import { CartDetailsComponent } from './cart-details/cart-details.component';
import { OrderProductsComponent } from './cart-details/order-products/order-products.component';
import { CreateOrderComponent } from './cart-details/create-order/create-order.component';
import { PaymentCompletedComponent } from './cart-details/payment-completed/payment-completed.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import {
  DlDateTimeDateModule,
  DlDateTimePickerModule
} from 'angular-bootstrap-datetimepicker';
import { ViewCardComponent } from './view-card/view-card.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { SubscriptionDialogComponent } from './subscription-dialog/subscription-dialog.component';
import { HotDealsComponent } from './hot-deals/hot-deals.component';
@NgModule({
  imports: [
    ShowHidePasswordModule,
    FormsModule,
    CommonModule,
    NgbModule,
    RouterModule.forChild(DashboardRoutes),
    DlDateTimeDateModule, // <--- Determines the data type of the model
    DlDateTimePickerModule,
    NgxMyDatePickerModule.forRoot(),
    MatButtonModule,
    MatDialogModule
  ],
  declarations: [
    HomeComponent,
    MealsComponent,
    SubscribeComponent,
    CheckoutComponent,
    SigninComponent,
    VerifyComponent,
    SignupComponent,
    ForgotPasswordComponent,
    UserNavigationComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ResetPasswordComponent,
    OrdersComponent,
    ManageAddressComponent,
    CakeComponent,
    CakeCheckoutComponent,
    CareersComponent,
    JobDescriptionComponent,
    MyGroceriesComponent,
    MyMealsComponent,
    OrderReceiptComponent,
    MealOrderReceiptComponent,
    CurrencySymbolPipe,
    GroceriesComponent,
    QuickComponent,
    CartDetailsComponent,
    OrderProductsComponent,
    CreateOrderComponent,
    PaymentCompletedComponent,
    ViewCardComponent,
    SubscriptionDialogComponent,
    HotDealsComponent
  ],
  entryComponents: [ViewCardComponent, SubscriptionDialogComponent],
  exports: [CurrencySymbolPipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {}

import { Routes } from '@angular/router';
import { AuthenticationGuard } from '@app/core';
import { HomeComponent } from '@app/dashboard/home/home.component';
import { MealsComponent } from '@app/dashboard/meals/meals.component';
import { SubscribeComponent } from '@app/dashboard/subscribe/subscribe.component';
import { CheckoutComponent } from '@app/dashboard/checkout/checkout.component';
import { SigninComponent } from '@app/dashboard/signin/signin.component';
import { SignupComponent } from '@app/dashboard/signup/signup.component';
import { ForgotPasswordComponent } from '@app/dashboard/forgot-password/forgot-password.component';
import { UserNavigationComponent } from '@app/dashboard/user-navigation/user-navigation.component';
import { ProfileComponent } from '@app/dashboard/profile/profile.component';

import { ChangePasswordComponent } from '@app/dashboard/change-password/change-password.component';
import { OrdersComponent } from '@app/dashboard/orders/orders.component';
import { ManageAddressComponent } from '@app/dashboard/manage-address/manage-address.component';
import { CakeComponent } from '@app/dashboard/cake/cake.component';
import { CakeCheckoutComponent } from '@app/dashboard/cake-checkout/cake-checkout.component';
import { CareersComponent } from '@app/dashboard/careers/careers.component';
import { JobDescriptionComponent } from '@app/dashboard/job-description/job-description.component';
import { AuthGuard } from '../services/auth.guard';
import { MyGroceriesComponent } from '@app/dashboard/my-groceries/my-groceries.component';
import { OrderReceiptComponent } from '@app/dashboard/order-receipt/order-receipt.component';
import { MealOrderReceiptComponent } from '@app/dashboard/meal-order-receipt/meal-order-receipt.component';
import { MyMealsComponent } from '@app/dashboard/my-meals/my-meals.component';
import { VerifyComponent } from '@app/dashboard/verify/verify.component';
import { ResetPasswordComponent } from '@app/dashboard/reset-password/reset-password.component';
import { GroceriesComponent } from './groceries/groceries.component';

import { QuickComponent } from './quick/quick.component';
import { CartDetailsComponent } from './cart-details/cart-details.component';
import { OrderProductsComponent } from './cart-details/order-products/order-products.component';
import { CreateOrderComponent } from './cart-details/create-order/create-order.component';
import { PaymentCompletedComponent } from './cart-details/payment-completed/payment-completed.component';
import { HotDealsComponent } from './hot-deals/hot-deals.component';

export const DashboardRoutes: Routes = [
  {
    path: '',
    // canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        component: GroceriesComponent,
        data: {
          title: 'Home'
        }
      },
      {
        path: 'home',
        component: GroceriesComponent,
        data: {
          title: 'Home'
        }
      },
      {
        path: 'meals',
        component: MealsComponent,
        data: {
          title: 'Meals'
        }
      },
      {
        path: 'store/:storeName',
        component: GroceriesComponent,
        data: {
          title: 'Store'
        }
        // canActivate: [AuthGuard]
      },
      {
        path: 'quick/:storeName',
        component: QuickComponent,
        data: {
          title: 'Quick'
        }
        // canActivate: [AuthGuard]
      },
      {
        path: 'cart',
        component: CartDetailsComponent,
        data: {
          title: 'Cart'
        }
        // canActivate: [AuthGuard]
      },
      {
        path: 'cart/checkout',
        component: CreateOrderComponent,
        data: {
          title: 'Checkout'
        }
        // canActivate: [AuthGuard]
      },
      {
        path: 'cart/payment-completed',
        children: [
          {
            path: '**',
            component: PaymentCompletedComponent
          }
        ],
        // component: PaymentCompletedComponent,
        data: {
          title: 'Payment Status'
        }
        // canActivate: [AuthGuard]
      },
      {
        path: 'subscribe',
        component: SubscribeComponent,
        data: {
          title: 'Subscribe'
        }
      },
      {
        path: 'checkout',
        component: CheckoutComponent,
        data: {
          title: 'Checkout'
        }
      },
      {
        path: 'signin',
        component: SigninComponent,
        data: {
          title: 'Signin'
        }
      },
      {
        path: 'verify/:id',
        component: VerifyComponent,
        data: {
          title: 'Verify'
        }
      },
      {
        path: 'signup',
        component: SignupComponent,
        data: {
          title: 'Signup'
        }
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        data: {
          title: 'Forgot Password'
        }
      },
      {
        path: 'user-navigation',
        component: UserNavigationComponent,
        data: {
          title: 'User Navigation'
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {
          title: 'Profile'
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
        data: {
          title: 'Change Password'
        },
        canActivate: [AuthGuard]
      },

      {
        path: 'reset-password/:id',
        component: ResetPasswordComponent,
        data: {
          title: 'Reset Password'
        }
      },

      {
        path: 'orders',
        component: OrdersComponent,
        data: {
          title: 'Orders'
        },
        canActivate: [AuthGuard]
      },

      {
        path: 'order-receipt/:id',
        component: OrderReceiptComponent,
        data: {
          title: 'Order Receipt'
        }
        // canActivate: [AuthGuard]
      },

      {
        path: 'meal-order-receipt/:id',
        component: MealOrderReceiptComponent,
        data: {
          title: 'Meal Order Receipt'
        }
        // canActivate: [AuthGuard]
      },

      {
        path: 'my-groceries/:storeName',
        component: MyGroceriesComponent,
        data: {
          title: 'My Groceries'
        },
        canActivate: [AuthGuard]
      },

      {
        path: 'my-meals',
        component: MyMealsComponent,
        data: {
          title: 'My Meals'
        },
        canActivate: [AuthGuard]
      },

      {
        path: 'manage-address',
        component: ManageAddressComponent,
        data: {
          title: 'Manage Address'
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'cake',
        component: CakeComponent,
        data: {
          title: 'Cake'
        }
      },
      {
        path: 'cake-checkout',
        component: CakeCheckoutComponent,
        data: {
          title: 'Checkout'
        }
      },
      {
        path: 'careers',
        component: CareersComponent,
        data: {
          title: 'Careers'
        }
      },
      {
        path: 'job-description/:id',
        component: JobDescriptionComponent,
        data: {
          title: 'Job Description'
        }
      },
      {
        path: 'hot-deals',
        component: HotDealsComponent,
        data: {
          title: 'Hot Deals'
        }
      }
    ]
  }
];

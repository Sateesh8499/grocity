import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  customerDetailsArr: any = [];
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    public auth: AuthService,
  ) {

  }
  ngOnInit() {


  }

  public submitForm(form: any) {
    if (!form.value) {
      this.toastr.warning("Please fill in the required details!");
      return;
    }
    this.spinner.show();
    let data = JSON.stringify({
      email: form.value.email,
      password: form.value.password,
    })

    this.auth.checkMemberAccountExists(data).subscribe((res: any) => {
      this.spinner.hide();
      if (res.status === 'success') {
        localStorage.setItem('currentUserProfile', JSON.stringify(res.message));
        this.toastr.success("Welcome! " + res.message.name);
        this.auth.user$ = res.message;
        this.router.navigate(['/user-navigation']);
      } else {

        if (res.data) {
          if (res.data === "email_verification_sent") {
            form.reset();
            $(".contact__msg").show();
            $("#formDiv").hide();
            this.toastr.success("Check your inbox to verify your email address!");

          }
        } else {
          this.auth.user$ = [];
          this.toastr.error(res.message);
        }


      }

    },
      (err: any) => {
        this.spinner.hide();
        this.toastr.warning("Something went wrong!");
      }
    );
  }



}

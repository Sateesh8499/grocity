import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  customerDetailsArr: any = [];
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    public auth: AuthService,

  ) {
    this.customerDetailsArr = this.auth.user$;

  }
  ngOnInit() {


  }


  public submitForm(form: any) {
    if (!form.value) {
      this.toastr.warning("Please fill in the required details!");
      return;
    }

    if (form.value.new_password.trim()) {
      var new_password = form.value.new_password.trim();
      if (new_password.length < 6) {
        this.toastr.error("Please enter atleast 6 characters");
        this.spinner.hide();
        return false;
      }
    }


    this.spinner.show();
    let data = JSON.stringify({
      id: form.value.id,
      email: this.customerDetailsArr.email,
      password: form.value.password,
      new_password: form.value.new_password
    })

    this.auth.changeMemberPassword(data).subscribe((res: any) => {
      this.spinner.hide();
      if (res.status === 'success') {
        //localStorage.setItem('currentUserProfile', JSON.stringify(res.message));
        //this.auth.user$ = res.message;
        this.auth.user$ = [];
        this.toastr.success("Password changed, login again!");
        this.router.navigate(['/signin']);
      } else {
        //  this.auth.user$ = [];
        this.toastr.error(res.message);
        return;
      }

    },
      (err: any) => {
        this.spinner.hide();
        this.toastr.warning("Something went wrong!");
      }
    );
  }


}

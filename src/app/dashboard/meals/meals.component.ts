import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import * as $ from 'jquery';
declare var jQuery: any;
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { StoreService } from '../../services/store.service';

import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.scss']
})
export class MealsComponent implements OnInit {
  constructor(
    private ref: ChangeDetectorRef,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    public router: Router,
    private titleService: Title,
    private metaService: Meta
  ) {
    // console.log('hi');
  }
  title = 'Not Jus A` Salad | MEALS';

  activate = false;
  activateText = 'Fetching time...';
  subscriptionPeriodArr: any;
  now: any;
  end: any;
  date: any;

  goto() {
    window.open(
      'https://docs.google.com/forms/d/e/1FAIpQLSdfB87qDwWSxB5TlAnftOqbtrJWtC8rZ-nfGXFA2xH01qofgg/viewform',
      '_blank'
    );
  }
  nextDay(day: any) {
    const d = new Date();
    (day = (Math.abs(+day || 0) % 7) - d.getDay()) < 0 && (day += 7);
    return day && d.setDate(d.getDate() + day), d;
  }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.metaService.addTags([
      {
        name: 'keywords',
        content:
          'organic,food,marketing,shop,product,organic food,farmer,agriculture,fruits,vegetables,healthy food,resposnsive,Fruits Shop, vegetable store, Ecommerce, Stores, Orders, Payments, Groceries, Clothings, Shopping'
      },
      {
        name: 'description',
        content: `Healthy Eating made easy by Meal Planning, Jamaica's Most Popular Daily/Weekly Meal Kit, Visit our store, browse menus and build your order in seconds`
      },
      { name: 'robots', content: 'index, follow' }
    ]);

    // console.log('hi');
    this.getsubscriptionPeriodDetails();
  }
  getsubscriptionPeriodDetails() {
    const data = {
      store_id: environment.storeId
    };

    this.spinner.show();
    this.store.getsubscriptionPeriodDetails(data).subscribe(
      (res: any) => {
        // console.log('hi');
        if (res.status === 'success') {
          if (res.message) {
            this.subscriptionPeriodArr = res.message;
            this.now = res.now;
            this.end = res.end;
            this.date = res.date;

            let i = 0;
            const x = setInterval(() => {
              let countDownDate = new Date().getTime();

              if (this.end) {
                countDownDate = this.end;
              }

              let now = new Date().getTime();
              // 1c.
              function changeTimezone(date: any, timeZone: any) {
                // suppose the date is 12:00 UTC
                const invdate = new Date(
                  date.toLocaleString('en-US', {
                    timeZone: timeZone
                  })
                );
                // then invdate will be 07:00 in Toronto
                // and the diff is 5 hours
                const diff = date.getTime() - invdate.getTime();
                // so 12:00 in Toronto is 17:00 UTC
                return new Date(date.getTime() - diff); // needs to substract
              }

              if (this.now) {
                i = i + 1000;
                now = this.now + i;
              }

              // Find the distance between now and the count down date
              const distance = countDownDate - now;

              // Time calculations for days, hours, minutes and seconds
              const days = Math.floor(distance / (1000 * 60 * 60 * 24));
              const hours = Math.floor(
                (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
              );
              const minutes = Math.floor(
                (distance % (1000 * 60 * 60)) / (1000 * 60)
              );
              const seconds = Math.floor((distance % (1000 * 60)) / 1000);

              // Display the result in the element with id="demo"
              // document.getElementById("demo").innerHTML = days + "d " + hours + "h "

              // $("#demo").html(days + ((days === 1) ? "day " : "days ") + hours + ((hours === 1) ? "day " : "hours ") + minutes + ((minutes === 1) ? "minute " : "minutes ") + seconds + ((seconds === 1) ? "second " : "seconds ") + " Left.");
              this.activateText =
                days +
                (days === 1 ? 'day ' : 'days ') +
                hours +
                (hours === 1 ? 'hour ' : 'hours ') +
                minutes +
                (minutes === 1 ? 'minute ' : 'minutes ') +
                seconds +
                (seconds === 1 ? 'second ' : 'seconds ') +
                ' Left.';

              // console.log(this.activateText);
              // If the count down is finished, write some text
              if (distance <= 0) {
                clearInterval(x);
                this.activate = false;
                this.activateText = 'MEAL PLAN REGISTRATION CLOSED!';
                $('#subscribeBtn').attr('disabled', 'true');

                //   $("#demo").html("MEAL PLAN REGISTRATION CLOSED!");
              } else {
                $('#subscribeBtn').removeAttr('disabled');

                this.activate = true;
              }
            }, 1000);
          } else {
            this.subscriptionPeriodArr = [];
          }
          this.spinner.hide();
        } else {
          this.toastr.warning('Something went wrong!');
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}

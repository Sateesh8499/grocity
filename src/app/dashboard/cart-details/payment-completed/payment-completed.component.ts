import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencySymbolPipe } from '@app/currency-symbol.pipe';
import { AuthService } from '@app/services/auth.service';
import { CommonService } from '@app/services/common.service';
import { GroceriesStoreService } from '@app/services/groceries.service';
import { StorageService } from '@app/services/storage.service';
import { StoreService } from '@app/services/store.service';
import { environment } from '@env/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import swal from 'sweetalert2';
import { switchMap } from "rxjs/operators" // RxJS v6

@Component({
  selector: 'app-payment-completed',
  templateUrl: './payment-completed.component.html',
  styleUrls: ['./payment-completed.component.css']
})
export class PaymentCompletedComponent implements OnInit {
  constructor(
    private store: GroceriesStoreService,
    // private modalController: ModalController,
    private spinner: NgxSpinnerService,
    public storageService: StorageService,
    public common: CommonService,
    private ref: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    public store2: StoreService,
    private toastr: ToastrService,
    private auth: AuthService
  ) {

  }
  searchParams: any;
  getUrlQueryParams(url: any) {
    var queryString = url.split("?")[1];
    var keyValuePairs = queryString.split("&");
    var keyValue = [];
    var queryParams: any = {};
    keyValuePairs.forEach(function (pair: any) {
      keyValue = pair.split("=");
      queryParams[keyValue[0]] = decodeURIComponent(keyValue[1]).replace(/\+/g, " ");
    });
    return queryParams;
  }
  ngOnInit() {

    this.searchParams = this.getUrlQueryParams(this.router.url);
    // console.table(this.searchParams);
    this.updatePaymentStatus();


  }


  updatePaymentStatus() {
    if (!this.searchParams || this.searchParams === '') {
      return;
    }
    const getStoreName = localStorage.getItem('storeName');

    const data = {
      store_name: getStoreName,
      pay_online_response: this.searchParams,
      developer_id: environment.developer_id
    };

    this.spinner.show();
    this.store.updatePaymentStatus(data).subscribe(
      (res: any) => {
        // this.toastr.success('Payment process completed!');
        this.spinner.hide();
      },
      (err: any) => {
        this.spinner.hide();
      }
    );
  }



  ionViewDidEnter() { }



}

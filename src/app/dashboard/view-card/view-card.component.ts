import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-card',
  templateUrl: './view-card.component.html',
  styleUrls: ['./view-card.component.css']
})
export class ViewCardComponent implements OnInit {
  imgLoaded: any = false;
  product: any;
  fromDialog: string;
  storeData: any;
  constructor(
    public dialogRef: MatDialogRef<ViewCardComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    @Optional() @Inject(MAT_DIALOG_DATA) public ssData: any
  ) {
    this.product = data.pageValue;
    this.storeData = ssData.sData;
  }
  ngOnInit() {}
  closeDialog() {
    this.dialogRef.close({ event: 'close', data: this.fromDialog });
  }
}

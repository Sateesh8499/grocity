import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';

import swal from 'sweetalert2';
import { CurrencySymbolPipe } from '../../currency-symbol.pipe';
@Component({
  selector: 'app-meal-order-receipt',
  templateUrl: './meal-order-receipt.component.html',
  styleUrls: ['./meal-order-receipt.component.scss']
})
export class MealOrderReceiptComponent implements OnInit {
  orderDetails: any = [];
  ordersObj: any;
  ordersTotal = 0;
  id: any;
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    public auth: AuthService
  ) {}
  ngOnInit() {
    /*trying to eliminated # from url*/
    // console.log(this.router.url.split('/')[2]);

    if (this.router.url.split('/')[2]) {
      this.id = this.router.url.split('/')[2];
      // console.log(this.id, 'Id');
    }
    /*trying to eliminated # from url*/

    this.viewCustomerMealOrderDetailsByOrderID();
  }
  viewCustomerMealOrderDetailsByOrderID() {
    const data = {
      store_name: environment.mealsStoreId,
      order_id: this.id
    };
    // console.log(data, 'data');
    this.spinner.show();
    this.store.viewCustomerMealOrderDetailsByOrderID(data).subscribe(
      (res: any) => {
        // console.log(res, 'res');
        if (res.status === 'success') {
          this.orderDetails = res.message.order;
          this.spinner.hide();
        } else {
          this.orderDetails = [];
          this.spinner.hide();
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }

  cancelOrder(id: any, storeId: any) {
    swal
      .fire({
        title: 'Cancel Order',
        text: 'Are you sure?',
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, cancel it!'
      })
      .then(result => {
        if (result.value) {
          this.spinner.show();
          this.store.canceMealsOrder(id, 'cancelled', storeId).subscribe(
            (res: any) => {
              this.spinner.hide();
              if (res.status === 'success') {
                $('#cancelOrder').hide();
                this.toastr.success(res.message);
              } else {
                this.toastr.error(res.message);
                return;
              }
            },
            (err: any) => {
              this.spinner.hide();
              this.toastr.warning('Something went wrong');
            }
          );
        }
      });
  }
}

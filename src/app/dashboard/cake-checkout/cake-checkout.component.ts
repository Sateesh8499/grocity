import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-cake-checkout',
  templateUrl: './cake-checkout.component.html',
  styleUrls: ['./cake-checkout.component.scss']
})
export class CakeCheckoutComponent implements OnInit {
  subscriptionData: any;
  storeArr: any = [];
  customerDetailsArr: any = [];
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    public auth: AuthService
  ) {
    this.customerDetailsArr.address2 = 'none';
    this.customerDetailsArr.bank_option = 'none';
    this.customerDetailsArr.quantity = 1;
    this.customerDetailsArr.address2 = 'Jamaica';
    this.customerDetailsArr.parish = 'KINGSTON';
    this.customerDetailsArr.delivery_day = '';

    if (this.auth.user$) {
      //  this.customerDetailsArr = this.auth.user$;
      this.customerDetailsArr.address1 = this.auth.user$.address1;
      this.customerDetailsArr.contactEmail = this.auth.user$.email;
      this.customerDetailsArr.contactNo = this.auth.user$.mobile;
      this.customerDetailsArr.name = this.auth.user$.name;
    }

    this.getStoreDetails();
  }

  preferenceCountry($event: any) {
    if ($event.target.value) {
      // ($event.target.value === 'd'
      if (this.customerDetailsArr.address2 === 'Jamaica') {
        this.customerDetailsArr.parish = 'KINGSTON';
      } else {
        this.customerDetailsArr.parish =
          'Port of Spain Delivery (Corporate area)';
      }
    }
  }

  preference($event: any) {
    if ($event.target.value) {
      // ($event.target.value === 'd'
      this.customerDetailsArr.delivery_day = '';
    }
  }

  normalDeliveryCharges = 0;
  chosedDeliveryOption: any;
  deliveryOption($event: any) {
    this.chosedDeliveryOption = this.storeArr.delivery_options[
      $event.target.value
    ];
    this.storeArr.store.delivery_charges = this.chosedDeliveryOption
      ? +this.chosedDeliveryOption.price
      : this.normalDeliveryCharges;
  }
  ngOnInit() {
    // this.subscriptionData = JSON.parse(this.localStorage.getItem('subscriptionData'));
    // if (!this.subscriptionData) {
    //   this.toastr.error("Please choose meal subscription plan & choice to cake-checkout.");
    //   this.router.navigate(['/subscribe']);
    // }
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  cakeDetailsArr: any;
  public submitOrder(form: any) {
    if (!form.value) {
      this.toastr.warning('Invalid Message!');
      return;
    }
    // let data = form.value;
    if (this.customerDetailsArr.bank_option === 'none') {
      this.toastr.error('Choose Preferred Bank Option!');
      return;
    }

    if (this.customerDetailsArr.address2 === 'none') {
      this.toastr.error('Choose City!');
      return;
    }

    // if (this.customerDetailsArr.delivery_day === "") {
    //   this.toastr.error("Choose Delivery/Pickup day!");
    //   return;
    // }

    if (form.value.contactEmail.trim() == '') {
      this.toastr.error('Please enter your email address');
      this.spinner.hide();
      return false;
    } else {
      // var reg = new RegExp(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/);
      // if (reg.test(form.value.contactEmail.trim()) == false) {
      //   this.toastr.error("Please enter valid email address");
      //   this.spinner.hide();
      //   return false;
      // }
    }

    if (form.value.contactNo.trim() == '') {
      this.toastr.error('Please enter your contact number');
      this.spinner.hide();
      return false;
    } else {
      var phoneNum = form.value.contactNo.trim().replace(/[^\d]/g, '');
      if (phoneNum.length > 6 && phoneNum.length < 12) {
      } else {
        this.toastr.error('Please enter valid contact number');
        this.spinner.hide();
        return false;
      }
    }

    // if (this.customerDetailsArr.address2) {
    //   if (this.customerDetailsArr.address2 === 'Trinidad') {
    //     this.customerDetailsArr.parish = "";
    //   }
    // }

    if (this.customerDetailsArr.parish) {
      if (this.customerDetailsArr.parish === 'ISLANDWIDE (via TARA Courier)') {
        this.storeArr.store.delivery_charges = 0;
        this.chosedDeliveryOption = false;
      }
    }

    let [key1, value1] = Object.entries(this.storeArr.products)[0];
    this.spinner.show();
    let data = JSON.stringify({
      account_id: this.storeArr.store.account_email_id,
      store_name: this.storeArr.store.name,
      email: this.storeArr.store.account_email_id,
      selected_delivery: 'd',
      name: form.value.name,
      contact_no: form.value.contactNo,
      contact_email: form.value.contactEmail,
      address1: form.value.address1,
      address2: form.value.address2,
      pincode: '',
      notes:
        (form.value.notes ? form.value.notes : 'N/A') +
        (this.customerDetailsArr.parish
          ? (this.customerDetailsArr.address2 === 'Jamaica'
              ? '\n\nParish - '
              : '\nLocation - ') + this.customerDetailsArr.parish
          : '') +
        (this.customerDetailsArr.parish1
          ? '\n\nISLANDWIDE Parish - ' + this.customerDetailsArr.parish1
          : ''),
      currency_symbol: this.storeArr.store.currency_symbol,
      total:
        +this.storeArr.products[key1].discount_price *
        +this.customerDetailsArr.quantity,
      delivery_charges:
        $('#selectedDelivery').val() === 'd'
          ? this.storeArr.store.delivery_charges
          : 0, //new
      delivery_options: this.chosedDeliveryOption
        ? this.chosedDeliveryOption
        : 'false', //new
      bank_options: this.storeArr.bank_options[form.value.bank_option]
        ? this.storeArr.bank_options[form.value.bank_option]
        : 'N/A', //new
      pickup_day: 'N/A', //new
      //delivery_day: "N/A",//new
      delivery_day: this.customerDetailsArr.delivery_day, //new
      is_coupon_applied: 'false', //new
      coupon_discount_value: 0, //new
      coupon_details: 'false', //new
      payable:
        +this.storeArr.products[key1].discount_price *
          +this.customerDetailsArr.quantity +
        (this.storeArr.store.delivery_charges
          ? this.storeArr.store.delivery_charges
          : 0), //new
      items: [
        {
          id: this.storeArr.products[key1].id,
          name: this.storeArr.products[key1].name,
          description: this.storeArr.products[key1].description,
          price: this.storeArr.products[key1].price,
          discount_price: this.storeArr.products[key1].discount_price,
          images: this.storeArr.products[key1].images1,
          images1: this.storeArr.products[key1].images1,
          max_order_quantity: this.storeArr.products[key1].max_order_quantity,
          min_order_quantity: this.storeArr.products[key1].min_order_quantity,
          quantity: this.customerDetailsArr.quantity
        }
      ]
    });

    this.store.placeCakeOrder(data).subscribe(
      (res: any) => {
        this.spinner.hide();
        if (res.status === 'success') {
          form.reset();
          $('#quantity').attr('disabled', 'true');

          // if (this.customerDetailsArr.address2) {
          //   if (this.customerDetailsArr.address2 !== 'Trinidad') {
          //     $("#parish").attr("disabled", "true");
          //   }
          // }

          $('#parish').attr('disabled', 'true');

          $('#delivery_day').attr('disabled', 'true');

          $('.contact__msg').show();
          $('.formDiv').hide();

          this.toastr.success(res.message);
        } else {
          this.toastr.error(res.message);
        }
      },
      (err: any) => {
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }

  getStoreDetails() {
    const data = {
      store_id: environment.cakeStoreId
    };

    this.spinner.show();

    this.store.getStoreDetails(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          if (res.message) {
            this.storeArr = res.message;
            this.storeArr.bank_options = res.bank_options;
            this.storeArr.delivery_options = res.delivery_options;
            this.storeArr.delivery_days = res.delivery_days;

            if (this.storeArr.products) {
              let [key1, value1] = Object.entries(this.storeArr.products)[0];
              this.cakeDetailsArr = this.storeArr.products[key1];
            }

            if (this.storeArr.delivery_options) {
              //set delivery-option 0 as default
              let [key1, value1] = Object.entries(
                this.storeArr.delivery_options
              )[0];

              this.chosedDeliveryOption = this.storeArr.delivery_options[key1];
              this.storeArr.store.delivery_charges = this.chosedDeliveryOption
                ? +this.chosedDeliveryOption.price
                : this.normalDeliveryCharges;
              //set delivery-option 0 as default
            }
          } else {
            this.storeArr = [];
          }

          this.spinner.hide();
        } else {
          this.toastr.warning('Something went wrong!');
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}

import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';
import { CurrencySymbolPipe } from '../../currency-symbol.pipe';
import { Location } from '@angular/common';
@Component({
  selector: 'app-my-groceries',
  templateUrl: './my-groceries.component.html',
  styleUrls: ['./my-groceries.component.scss']
})
export class MyGroceriesComponent implements OnInit {
  customerGroceriesArr: any = [];
  ordersObj: any;
  ordersTotal = 0;
  storeName: any;
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    public auth: AuthService,
    private activatedRoute: ActivatedRoute,
    public location: Location
  ) {}
  goBack() {
    this.location.back();
  }
  ngOnInit() {
    this.getAllCustomerOrdersByEmailID();
  }
  getAllCustomerOrdersByEmailID() {
    this.activatedRoute.params.subscribe(res => {
      this.storeName = res.storeName;
      localStorage.setItem('storeName', this.storeName);
    });
    const data = {
      store_name: localStorage.getItem('storeName')
    };
    this.spinner.show();
    this.store.getAllCustomerOrdersByEmailID(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          this.ordersObj = res.message;

          this.customerGroceriesArr = [];
          //this.ordersObj = Object["values"](res.message);

          this.ordersTotal = 0;

          Object.keys(this.ordersObj).forEach(key => {
            this.ordersTotal =
              this.ordersTotal + +this.ordersObj[key].order.payable;
            this.customerGroceriesArr.push(this.ordersObj[key]);
          });

          this.customerGroceriesArr.reverse();

          this.spinner.hide();
        } else {
          this.customerGroceriesArr = [];
          this.spinner.hide();
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}

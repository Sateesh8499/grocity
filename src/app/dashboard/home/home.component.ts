import { Component, OnInit, Inject } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as $ from 'jquery';
import { WINDOW } from '@ng-toolkit/universal';
declare var jQuery: any;
declare var Swiper: any;
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title =
    'Not Jus A` Salad | Healthy Eating made easy by Meal Planning | Grocery Delivery and Salads';

  constructor(
    @Inject(WINDOW) private window: Window,
    private titleService: Title,
    private metaService: Meta
  ) {
    // console.log("Nandan");
  }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.metaService.addTags([
      {
        name: 'keywords',
        content:
          'organic,food,marketing,shop,product,organic food,farmer,agriculture,fruits,vegetables,healthy food,resposnsive,Fruits Shop, vegetable store, Ecommerce, Stores, Orders, Payments, Groceries, Clothings, Shopping'
      },
      {
        name: 'description',
        content: `Jamaica's #1 Healthy Option! Choose from Our Wide Selection of Healthy Food, when you shop online. You Do Not Have to Shop Around. Our Health Food Store Stocks a Variety of Delicious Meals, and natural farm produce.`
      },
      { name: 'robots', content: 'index, follow' }
    ]);

 
  }
}

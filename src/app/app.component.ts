import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { WINDOW } from '@ng-toolkit/universal';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title =
    'Not Jus A` Salad | Healthy Eating made easy by Meal Planning | Grocery Delivery and Salads';

  constructor(
    private titleService: Title,
    private metaService: Meta,
    @Inject(WINDOW) private window: Window,
    private router: Router
  ) {
    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }

      this.window.scrollTo(0, 0);
    });
  }
  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.metaService.addTags([
      {
        name: 'keywords',
        content:
          'organic,food,marketing,shop,product,organic food,farmer,agriculture,fruits,vegetables,healthy food,resposnsive,Fruits Shop, vegetable store, Ecommerce, Stores, Orders, Payments, Groceries, Clothings, Shopping'
      },
      {
        name: 'description',
        content: `Jamaica's #1 Healthy Option! Choose from Our Wide Selection of Healthy Food, when you shop online. You Do Not Have to Shop Around. Our Health Food Store Stocks a Variety of Delicious Meals, and natural farm produce.`
      },
      { name: 'robots', content: 'index, follow' }
    ]);
    
  }

  goToTop() {
    this.window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  }
}

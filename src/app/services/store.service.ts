import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import {
  environment,
  httpBasicAuthOptions,
  httpBasicOptions
} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  getStoreDetails(data: any): Observable<any> {
    const requestData = JSON.stringify({
      store_id: data.store_id
    });
    const url = environment.rootCloudUrl + 'getStoreDetails';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }



  payOnline(data: any): Observable<any> {

    const url = 'https://sandbox.wipayfinancial.com/v1/gateway';
    return this.http
      .post(url, data)
      .pipe(map((response: Response) => {


      response


      }
      )


      );
  }
  getsubscriptionPeriodDetails(data: any): Observable<any> {
    const requestData = JSON.stringify({
      store_id: data.store_id
    });
    const url = environment.rootCloudUrl + 'getsubscriptionPeriodDetails';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  getsubscriptionSummary(data: any): Observable<any> {
    const requestData = JSON.stringify({
      store_id: data.store_id
    });
    const url = environment.rootCloudUrl + 'getsubscriptionSummary';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  placeSubscriptionOrder(data: any): Observable<any> {
    const url = environment.rootCloudUrl + 'createSubscriptionOrderWithReceipt';
    return this.http
      .post(url, data, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }


  placeSubscriptionWithSideOrder(data: any): Observable<any> {
    const url = environment.rootCloudUrl + 'createSubscriptionOrderWithSideWithReceipt';
    return this.http
      .post(url, data, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }

  placeCakeOrder(data: any): Observable<any> {
    const url = environment.rootCloudUrl + 'createCakeOrderWithReceipt';
    return this.http
      .post(url, data, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }
  getAllCustomerOrdersByEmailID(data: any): Observable<any> {
    const requestData = JSON.stringify({
      store_name: data.store_name,
      email: JSON.parse(localStorage.getItem('currentUserProfile')).email
    });
    const url = environment.rootCloudUrl + 'getAllCustomerOrdersByEmailID';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  getAllCustomerMealOrdersByEmailID(data: any): Observable<any> {
    const requestData = JSON.stringify({
      store_name: data.store_name,
      email: JSON.parse(localStorage.getItem('currentUserProfile')).email
    });
    const url = environment.rootCloudUrl + 'getAllCustomerMealOrdersByEmailID';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  viewCustomerOrderDetailsByOrderID(data: any): Observable<any> {
    const requestData = JSON.stringify({
      order_id: data.order_id,
      store_name: data.store_name,
      email: JSON.parse(localStorage.getItem('currentUserProfile')).email
    });
    const url = environment.rootCloudUrl + 'viewCustomerOrderDetailsByOrderID';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  checkEmailVerificationCode(data: any): Observable<any> {
    const requestData = JSON.stringify({
      verification_code: data.verification_code
    });
    const url = environment.rootCloudUrl + 'checkEmailVerificationCode';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  checkMobileVerification(data: any): Observable<any> {
    const requestData = JSON.stringify({
      id: data.id,
      mobile: data.mobile
    });
    const url = environment.rootCloudUrl + 'checkMobileVerification';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  checkResetPasswordCode(data: any): Observable<any> {
    const requestData = JSON.stringify({
      verification_code: data.verification_code
    });
    const url = environment.rootCloudUrl + 'checkResetPasswordCode';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  viewCustomerMealOrderDetailsByOrderID(data: any): Observable<any> {
    const requestData = JSON.stringify({
      order_id: data.order_id,
      store_name: data.store_name,
      email: JSON.parse(localStorage.getItem('currentUserProfile')).email
    });
    const url =
      environment.rootCloudUrl + 'viewCustomerMealOrderDetailsByOrderID';
    return this.http
      .post(url, requestData, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  cancelGroceriesOrder(id: any, status: any, storeId: any): Observable<any> {
    const url =
      environment.rootCloudUrl +
      'cancelGroceriesOrder?store_id=' +
      storeId +
      '&order_id=' +
      id +
      '&order_status=' +
      status;
    return this.http
      .delete(url, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }

  canceMealsOrder(id: any, status: any, storeId: any): Observable<any> {
    const url =
      environment.rootCloudUrl +
      'canceMealsOrder?store_id=' +
      storeId +
      '&order_id=' +
      id +
      '&order_status=' +
      status;
    return this.http
      .delete(url, httpBasicOptions)
      .pipe(map((response: Response) => response));
  }
}

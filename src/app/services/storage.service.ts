import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment, httpOptions } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../services/common.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  storage: any;
  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private common: CommonService
  ) {
    this.storage = {
      setItem: function(key: any, value: any) {
        return Promise.resolve().then(function() {
          localStorage.setItem(key, JSON.stringify(value));
        });
      },
      getItem: function(key: any) {
        return Promise.resolve().then(function() {
          return JSON.parse(localStorage.getItem(key));
        });
      }
    };
  }

  getStorage(ITEMS_KEY: any): Promise<any[]> {
    return this.storage.getItem(ITEMS_KEY);
  }

  setStorageValueOld(product: any, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any[]) => {
      if (items) {
        items.push(product);
        return this.storage.setItem(ITEMS_KEY, items);
      } else {
        return this.storage.setItem(ITEMS_KEY, [product]);
      }
    });
  }

  setStorageValue(product: any, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any) => {
      if (items) {
        let isNew = true;
        let newItems: any[] = [];
        let k = 0;
        for (const i of items['cartItems']) {
          if (i.productDto.productId === product.id) {
            const newQuantity = i.quantity + 1;
            i.quantity = newQuantity;
            isNew = false;
            product.quantity = newQuantity;
            product = product;
          }
          k++;
        }

        if (isNew) {
          items['cartItems'].push(product);
        }

        newItems = items;

        let cartId = '';
        if (localStorage.getItemItem('cartId')) {
          cartId = localStorage.getItemItem('cartId');
        }

        const data = {
          cartId: cartId,
          productId: product.id,
          quantity: product.quantity
        };
        this.addToCart(data).subscribe(
          (res: any) => {
            this.spinner.hide();

            // localStorage.setItemItem('cartId', res.cartId),
            this.toastr.success('Added to cart!');
            return this.storage.setItem(ITEMS_KEY, res);
          },
          (err: any) => {
            this.spinner.hide();
            if (!this.common.checkValidAuthResponseCode(err)) {
              return;
            }

            if (err.error.text) {
              this.toastr.success(err.error.text);
            } else {
              this.toastr.success(err.error.message);
            }
          }
        );
      } else {
        // console.log(ITEMS_KEY);

        let cartId = '';
        if (localStorage.getItemItem('cartId')) {
          cartId = localStorage.getItemItem('cartId');
        }

        const data = {
          cartId: cartId,
          productId: product.id,
          quantity: product.quantity
        };
        this.addToCart(data).subscribe(
          (res: any) => {
            //  console.log(res);
            this.spinner.hide();

            localStorage.setItemItem('cartId', res.cartId),
              this.toastr.success('Added to cart!');
            return this.storage.setItem(ITEMS_KEY, res);
          },
          (err: any) => {
            //  console.log(err.error);
            this.spinner.hide();
            if (!this.common.checkValidAuthResponseCode(err)) {
              return;
            }

            if (err.error.text) {
              this.toastr.success(err.error.text);
            } else {
              this.toastr.success(err.error.message);
            }
          }
        );
      }
    });
  }

  updateStorageValue(item: any, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any) => {
      const newItems: any[] = [];

      for (const i of items) {
        if (i.productDto.productId === item.id) {
          newItems.push(item);
        } else {
          newItems.push(i);
        }
      }

      let cartId = '';
      if (localStorage.getItemItem('cartId')) {
        cartId = localStorage.getItemItem('cartId');
      }

      const data = {
        cartId: cartId,
        productId: item.id,
        quantity: item.quantity
      };
      this.addToCart(data).subscribe(
        (res: any) => {
          // console.log(res);
          this.spinner.hide();
          // localStorage.setItemItem('cartId', res.cartId),
          this.toastr.success('Updated cart!');
          return this.storage.setItem(ITEMS_KEY, newItems);
        },
        (err: any) => {
          // console.log(err.error);
          this.spinner.hide();
          if (!this.common.checkValidAuthResponseCode(err)) {
            return;
          }

          if (err.error.text) {
            this.toastr.success(err.error.text);
          } else {
            this.toastr.success(err.error.message);
          }
        }
      );
    });
  }

  removeStorageValueOld(id: number, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      const toKeep: any[] = [];

      for (const i of items) {
        if (i.id !== id) {
          toKeep.push(i);
        }
      }
      return this.storage.setItem(ITEMS_KEY, toKeep);
    });
  }

  removeStorageValue(id: number, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      let k = 0;
      for (const i of items) {
        if (i.id === id) {
          items.splice(k, 1);
        }
        k++;
      }

      // console.table(items);

      if (!items || items.length === 0) {
        return this.storage.remove(ITEMS_KEY);
      } else {
        return this.storage.setItem(ITEMS_KEY, items);
      }
    });
  }

  addToCart(data: any): Observable<any> {
    const requestData = JSON.stringify({
      cartId: '',
      discountCode: '',
      offer: false,
      productId: data.productId,
      quantity: data.quantity,
      storeId: localStorage.getItemItem('storeId'),
      token: {
        fingerprint: {
          createdAt: 0,
          deviceFingerprint: localStorage.getItemItem('deviceFingerPrint'),
          jsonOtherInfo: '',
          user_id: 0
        },
        loginToken: localStorage.getItemItem('loginToken')
      }
    });

    const url = environment.rootCloudUrl + 'store/addToCart';
    const t = JSON.parse(localStorage.getItemItem('currentUserProfile'));
    // headers.append("Authorization", "Bearer " + t);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + t.accessToken
      })
    };
    return this.http
      .post(url, requestData, httpOptions)
      .pipe(map((response: Response) => response));
  }

  getStorageCOF(ITEMS_KEY: any): Promise<any[]> {
    return this.storage.getItem(ITEMS_KEY);
  }

  setStorageValueCOF(product: any, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any[]) => {
      if (items) {
        let isNew = true;
        let newItems: any[] = [];
        let k = 0;
        for (const i of items) {
          if (i.id === product.id) {
            const newQuantity = i.quantity + 1;
            i.quantity = newQuantity;
            isNew = false;
            product.quantity = newQuantity;
            product = product;
          }
          k++;
        }

        if (isNew) {
          product.quantity = product.min_order_quantity
            ? +product.min_order_quantity
            : product.quantity;

          items.push(product);
        }
        newItems = items;
        return this.storage.setItem(ITEMS_KEY, newItems);
      } else {
        return this.storage.setItem(ITEMS_KEY, [product]);
      }
    });
  }

  updateStorageValueCOF(item: any, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      const newItems: any = [];

      for (const i of items) {
        if (i.id === item.id) {
          newItems.push(item);
        } else {
          newItems.push(i);
        }
      }

      return this.storage.setItem(ITEMS_KEY, newItems);
    });
  }

  removeStorageValueCOF(id: number, ITEMS_KEY: any): Promise<any> {
    return this.storage.getItem(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      const toKeep: any = [];

      for (const i of items) {
        if (i.id !== id) {
          toKeep.push(i);
        }
      }
      return this.storage.setItem(ITEMS_KEY, toKeep);
    });
  }

  clearStorageValueCOF(ITEMS_KEY: any): Promise<any> {
    return this.storage.setItem(ITEMS_KEY, []);
  }

  clearStorageValueALL(): Promise<any> {
    return this.storage.clear();
  }

  setCustomerStorageValueCOF(
    customerDetails: any,
    ITEMS_KEY: any
  ): Promise<any> {
    return this.storage.setItem(ITEMS_KEY, [customerDetails]);
  }
}

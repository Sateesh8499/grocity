import { HttpHeaders } from '@angular/common/http';

export const username = '!njas!';
export const password = '!njas@123!';
export const BasicAuth = 'Basic ' + btoa(username + ':' + password);
export const httpBasicAuthOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: BasicAuth
  })
};

export const environment = {
  production: false,
  rootCloudUrl: 'https://us-central1-notjusasalad-df110.cloudfunctions.net/',
  //rootCloudUrl: 'https://api.notjusasalad.com/',
  rootStoreUrl: 'http://groceries.notjusasalad.com/',
  baseStoreUrl: 'notjusasalad.com',
  storeId: 'meals',
  storeEmail: 'meals.notjusasalad@gmail.com',
  cakeStoreId: 'christmascake',
  groceriesStoreId: 'demogroceries',
  mealsStoreId: 'meals',
  cakeStoreEmail: 'christmascake.notjusasalad@gmail.com',
  globalPushNotificationsTopic: 'cof_app',

  //  developer_id: 6813560715,
  // wipay_payment_gateway_url: 'https://wipayjm.com/v1/gateway_live',
  //  payment_return_url: 'http://notjusasalad.com/cart/payment-completed',

  developer_id: 1, //sanbox
  wipay_payment_gateway_url: 'https://sandbox.wipayfinancial.com/v1/gateway', //sanbox
  payment_return_url: 'http://localhost:4200/cart/payment-completed',

};

export const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
export const httpBasicOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Basic ' + btoa('!njas!' + ':' + '!njas@123!')
  })
};
